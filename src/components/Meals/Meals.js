
import MealsSummery from './MealsSummary';
import AvailableMelas from './AvailableMeals';
import { Fragment } from 'react';

const Meals = () => {
    return (
        <Fragment>
            <MealsSummery/>
            <AvailableMelas/>
        </Fragment>

    );
};

export default Meals;